#include <wiringPi.h>
#include <pcf8591.h>
#include <stdio.h>
#include<time.h>

#define Address 0x48
#define BASE 64
#define A0 BASE+0
#define A1 BASE+1
#define A2 BASE+2
#define A3 BASE+3
#define D3 26

int main()
{
        time_t now;
	struct tm *timenow;
    time(&now);
    timenow = localtime(&now);
    printf("%d\t",timenow->tm_mday);
    printf("%d\t",timenow->tm_mon + 1);
    printf("%d\t",timenow->tm_year + 1900);
    printf("%d\t",timenow->tm_hour);
    printf("%d\t",timenow->tm_min);
    printf("%d\t",timenow->tm_sec);



    if (wiringPiSetup() < 0) {
        printf("%1d\t%1d\t%1d\t%1d\n",-1,-1,-1,-1);
        return 0;
    } else {
        pcf8591Setup(BASE,Address);   

        printf("%1d\t%1d\t%1d\t%1d\n"
            ,analogRead(A0)
            ,analogRead(A1)
            ,analogRead(A2)
            ,analogRead(A3)
            );
        return 1;
    };
}
