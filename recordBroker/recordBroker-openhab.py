#!/usr/bin/env python
import paho.mqtt.client as mqtt
from datetime import datetime
from time import gmtime, strftime, time
import os
import re
import json
import csv


rootDir = '/home/openhabian/recordBroker'
broker = 'openhab'

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError, e:
    return False
  return True
  
  
  
def timeStamp():
    #return(strftime("%d\t%d\t%m\t%Y\t%H\t%M\t%S", [time(),gmtime()])) # UTC
    risposta =  strftime("\t%d\t%m\t%Y\t%H\t%M\t%S", gmtime())
    
    return(risposta) # UTC

def topic2dir(topic, k):
    livelli = re.split(r'/',topic)
    if k == 1 :
        directory = rootDir + '/' + broker + '/' + livelli[k-1]
    elif k == 2 : 
        directory = rootDir + '/' + broker + '/' + livelli[k-2] + '/' + livelli[k-1]

    if (not os.path.isdir(directory)) :
        print(directory)
        os.mkdir(directory)
    return directory

def topic2filename(topic, k):
    livelli = re.split(r'/',topic)
    if (len(livelli)>k-1):
        if k == 2 :
            return livelli[k-1]
        elif k == 3 :
            return livelli[k-2] + '-' + livelli[k-1]
    else:
        return ""


def on_connect(client, userdata, flags, rc):
    risposta = '[{"recordtime": {"unixtime" : %4.3f' % time()
    risposta = risposta + strftime(' , "gmt": 0 , "day" : %-d , "month" : %-m , "year" : %Y , "hour" : %-H,"minutes" : %-M , "seconds" : %-S} ', gmtime())
    risposta = risposta + ', "action" : "client.on_connect" , "rc" : '  +str(rc) + ' , "comment":"Connected with result code rc"}'
    #print(risposta)

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("#")
    
    
def on_message(client, userdata, msg):
    mygmtime = gmtime()
    mygmtimeIndex = [0,1,2,3,4,5]
   
    subdirDeep = 1
    
    mNexus  = re.search("rtl2me/Nexus/ch\d/",msg.topic)
    mMebus  = re.search("rtl2me/Mebus/ch\d/",msg.topic)
#    mRaspi  = re.search("raspi\d\d/",msg.topic)
    mXiaomi = re.search("Xiaomi/",msg.topic)
    mShellies = re.search("shellies/",msg.topic)
    mTasmotas = re.search("tasmotas/",msg.topic)

    if (mNexus  != None) : subdirDeep = 2
    if (mMebus  != None) : subdirDeep = 2
#    if (mRaspi  != None) : subdirDeep = 2
    if (mXiaomi != None) : subdirDeep = 2
    if (mShellies != None) : subdirDeep = 2
    if (mTasmotas != None) : subdirDeep = 2
    if (re.search("raspi02/Arduino/"    , msg.topic) != None) : subdirDeep = 2
    if (re.search("raspi32/sensori/"    , msg.topic) != None) : subdirDeep = 2
    if (re.search("ESP32/"              , msg.topic) != None) : subdirDeep = 2
    if (re.search("ESP8266/"            , msg.topic) != None) : subdirDeep = 2
    if (re.search("xavax/"              , msg.topic) != None) : subdirDeep = 2

    mydir = topic2dir(msg.topic,subdirDeep)
    myfilename = topic2filename(msg.topic , subdirDeep +1 ) + "-%04d%02d.tsv"%(mygmtime[0],mygmtime[1])
    fullfilename = mydir + '/' + myfilename
    if (not os.path.isfile(fullfilename)):
        print(fullfilename)
        with open(fullfilename, "w") as tsv:
            tw = csv.writer(tsv, delimiter="\t", lineterminator="\n", quotechar='', quoting = csv.QUOTE_NONE)
            tw.writerow(['time' , 'gmt' , 'year' , 'month' , 'day' , 'hour' , 'min' , 'sec' , 'topic' , 'type' , 'msg'])
        
    
    if msg.payload.isdigit():
        msg.payload = int(msg.payload)
        #tipo = '"integer"'
        tipo = 'integer'
    elif msg.payload.replace('.','',1).isdigit():
        msg.payload = float(msg.payload)
        #tipo = '"float"'
        tipo = 'float'
    elif is_json(msg.payload):
        msg.payload = msg.payload
        tipo = '"json"'
        tipo = 'json'
    else :
        msg.payload = str(msg.payload).replace('\\','\\\\').replace('"','\\"').replace('\t','\\t').replace('\n','\\n')
        msg.payload = '"' + msg.payload + '"'
        #tipo = '"string"'
        tipo = 'string'
    
    #risposta = ",{\"recordtime\" : {\"unixtime\" : %4.3f" % time()
    #risposta = risposta + strftime(' , "gmt": 0 , "day" : %-d , "month" : %-m , "year" : %Y , "hour" : %-H,"minutes" : %-M , "seconds" : %-S} ', gmtime())
    #risposta = risposta + ",\"topic\": \"" +  msg.topic + '", "payload": {"type" : ' + tipo + ',"message" :' + msg.payload + "}}"
    
    #                time (dello script)   :  gmt  :  year : month : day :  hour : min : sec 
    #risposta = "%4.3f" % time() + strftime('\t0\t%Y\t%-m\t%-d\t%-H\t%-M\t%-S', gmtime())
    #risposta = risposta + '\t' + msg.topic + '\t' + tipo + '\t' + msg.payload 
    
    #mygmtimeArray = [ mygmtime[i] for i in mygmtimeIndex]
    #          ['time' , 'gmt' , 'year'      , 'month'     , 'day'       , 'hour'      , 'min'       , 'sec'       , 'topic'   , 'type' , 'msg']
    risposta = [time() , 0     , mygmtime[0] , mygmtime[1] , mygmtime[2] , mygmtime[3] , mygmtime[4] , mygmtime[5] , msg.topic , tipo   ,  msg.payload]
    #print('\n' + mydir + '/' + myfilename + '\n===================================')
    #x = "#".join(risposta)
    #print("risposta")
    with open(fullfilename, "a") as tsv:
        tw = csv.writer(tsv, delimiter="\t", lineterminator="\n", quotechar='', quoting = csv.QUOTE_NONE)
        tw.writerow(risposta)

    




client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message



if (not os.path.isdir(rootDir)) :
    os.mkdir(rootDir)

if (not os.path.isdir(rootDir + '/' + broker)) :
    os.mkdir(rootDir + '/' + broker)


client.connect(broker, 1883, 60)



client.loop_forever()
