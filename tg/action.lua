function os.capture(cmd, raw)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  if raw then return s end
  s = string.gsub(s, '^%s+', '')
  s = string.gsub(s, '%s+$', '')
  s = string.gsub(s, '[\n\r]+', ' ')
  return s
end


function on_msg_receive (msg)
      messaggio = msg.text
      messaggio = string.gsub(messaggio, "  +"," ")
      if msg.out then
          return
      end
      if (msg.text=='CQ') then
          send_msg(msg.from.print_name, 'Ciao. Sono on-line e pronto ad eseguire i tuoi ordini (se di mio gradimento :-)', ok_cb, false)
          if not(msg.from.print_name =='Antonio_Gulino') then
              send_msg("Antonio_Gulino", 'Ciao. Sono on-line e pronto ad eseguire i tuoi ordini (se di mio gradimento :-)', ok_cb, false)
          end
      end
      if (msg.text=='ping') then
         send_msg (msg.from.print_name, 'pong', ok_cb, false)
      end
      if (msg.text=='ls') and (msg.from.print_name=="Antonio_Gulino") then
         send_msg (msg.from.print_name, os.capture("ls /home/pi",true), ok_cb, false)
      end

      if (msg.text=='sensori') and (msg.from.print_name=="Antonio_Gulino") then
         send_msg (msg.from.print_name, os.capture("tail -n 2 /home/pi/sensors/*",true), ok_cb, false)
         send_msg (msg.from.print_name, os.capture("tail -n 2 /home/pi/rtl2me/*",true), ok_cb, false)
      end
      i,j = string.find(msg.text,'sensore ')
      if (i==1) and (msg.from.print_name=="Antonio_Gulino") then
         sensore = string.gsub(msg.text,'sensore ','',1)
         comando = "cat /home/pi/*r*/" .. sensore .. "* | /home/pi/bin/unique.pl|tail -n 40"
         send_msg (msg.from.print_name, comando, ok_cb, false)
         send_msg (msg.from.print_name, os.capture(comando,true), ok_cb, false)
      end

      if (msg.text=='dingdong') and (msg.from.print_name=="Antonio_Gulino") then
         os.execute("/home/pi/bin/dingdong")
      end
      if (msg.text=='beep') and (msg.from.print_name=="Antonio_Gulino") then
         os.execute("/home/pi/bin/beep")
      end
      if ((messaggio=='reboot now') and (msg.from.print_name=="Antonio_Gulino")) then
         send_msg (msg.from.print_name, 'Ripeti il tuo ordine aggiungendo alla fine il codice 6_oU#Qi=', ok_cb, false)
      end
      if ((messaggio=='reboot now 6_oU#Qi=') and (msg.from.print_name=="Antonio_Gulino")) then
         send_msg (msg.from.print_name, 'Avvio la procedura di reboot! (speriamo bene...)', ok_cb, false)
      end
      if (messaggio=='help') then
         if (msg.from.print_name=="Antonio_Gulino") then
            send_msg (msg.from.print_name, 'Possibili comandi:\n\nhelp\nping\nCQ\n\ndingdong\nbeep\nls\nsensori\nsensore <nomesensore>\nreboot now', ok_cb, false)
         end
         if not(msg.from.print_name=="Antonio_Gulino") then
            send_msg (msg.from.print_name, 'Possibili comandi:\n\nhelp\nping\nCQ', ok_cb, false)
         end
      end
  end
   
  function on_our_id (id)
  end
   
  function on_secret_chat_created (peer)
  end
   
  function on_user_update (user)
  end
   
  function on_chat_update (user)
  end
   
  function on_get_difference_end ()
  end
   
  function on_binlog_replay_end ()
  end
