Installazione
=============
* installare `telegram-cli` (vedasi sotto)
* `sudo apt-get install screen` - serve per potere inviare messaggi mentre rimane in ascolto
* aggiungere la riga `su pi -c "/usr/bin/screen -dmS Telegram /home/pi/tg/bin/telegram-cli  -k /home/pi/tg/tg-server.pub -s /home/pi/tg/action.lua &"` con `sudo vi /etc/rc.local`
* creare/modificare il file `/home/pi/tg/action.lua`

Utilizzo
========
Con lo script `tg-send-msg <TelegramUserDestinatario> '<messaggio evtl. tra apici>'` si possono mandare messaggi, mentre telegram continua ad ascoltare

Installazione di `telegram-cli`
==============================
Seguire quanto descritto dal progetto  https://github.com/vysheng/tg facendo attenzione
al seguente problema noto

Soluzione di un problema noto
-----------------------------
Durante l'installazione di `telegram-cli`, prima di eseguire `./config` è necessario modificare il file `gvim ~/tg/tgl/mtproto-utils.c`
* togliere (o commentare con due slash `//`) le righe 101 e 115 (contengono `assert (0); // As long as nobody ever uses this code, assume it is broken.`)


Dipendenze
==========
lo script `tg-send-msg` (e pertanto `telegram-cli`) vengono usati da `readSerialUSB0.py` (nel progetto 433)

Fonti web
=========
Per l'installazione di `telegram-cli`
* https://github.com/vysheng/tg - ma attenzione: prima del `./configure` risolvere il problema noto
* http://www.mehr4u.de/telegram-raspberry-pi-installation-konfiguration-und-beispiele.html - soluzione al problema noto