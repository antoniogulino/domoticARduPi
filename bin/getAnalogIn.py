#!/usr/bin/python
# ad Aprile 2019 ho aggiunto codice per il protocollo MQTT

import smbus
import time
import datetime
import paho.mqtt.client as mqtt

#####################################################
# parte dedicata al protocollo MQTT
#####################################################
def on_connect(client, userdata, flags, rc):
    print("Connection returned result: "+connack_string(rc))
    
def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")

mqtt = mqtt.Client(client_id="134098", userdata="datichefroseservono" )
mqtt.will_set("sconnessioni/raspi32/getAnalogIn.py", payload="mi sono scollegato in modo disordinato poco dopo le "+time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), qos=2, retain=True)
mqtt.on_connect = on_connect
mqtt.on_disconnect = on_disconnect



# 2014-08-26 PCF8591-x.py

# Connect Pi 3V3 - VCC, Ground - Ground, SDA - SDA, SCL - SCL.

# ./PCF8591-x.py

bus = smbus.SMBus(1)

aout = 0

n = 20 # quante volte ripetere la misurazione
pausa = 0.04 # secondi tra una misurazione e l'altra
# 0 = temperatura 
# 1 = fotoresistenza
# 2 = fotoresistenza
# 3 = impazzito

v = [0.0,0.0,0.0,0.0]

for i in range(n):
    for pin in range(0,4):
	bus.write_byte_data(0x48,0x40 | ((pin+1) & 0x03), aout)
	v[pin] = v[pin] + bus.read_byte(0x48)
	time.sleep(pausa)

for pin in range(0,4):
    v[pin] = v[pin]/n

timeStamp = time.strftime("%d\t%m\t%Y\t%H\t%M\t%S", time.localtime()) 

riga = timeStamp + "\t" + str(v[0])+"\t"+str(v[1])+"\t"+str(v[2])+"\t"+str(v[3])

print riga

# solo adesso viene la parte MQTT

mqtt.connect("minipc")
mqtt.loop_start()
msg_info_0 = mqtt.publish("raspi32/sensori/analogIn/0", payload=""+str(v[0]) , qos=2, retain=True)
msg_info_1 = mqtt.publish("raspi32/sensori/analogIn/1", payload=""+str(v[1]) , qos=2, retain=True)
msg_info_2 = mqtt.publish("raspi32/sensori/analogIn/2", payload=""+str(v[2]) , qos=2, retain=True)
msg_info_3 = mqtt.publish("raspi32/sensori/analogIn/3", payload=""+str(v[3]) , qos=2, retain=True)
msg_info_t = mqtt.publish("raspi32/sensori/analogIn/timeStamp", payload=timeStamp , qos=2, retain=True)
if msg_info_0.is_published() == False:
	msg_info_0.wait_for_publish()
if msg_info_1.is_published() == False:
	msg_info_1.wait_for_publish()
if msg_info_2.is_published() == False:
	msg_info_2.wait_for_publish()
if msg_info_3.is_published() == False:
	msg_info_3.wait_for_publish()
if msg_info_t.is_published() == False:
	msg_info_t.wait_for_publish()
mqtt.loop_stop()
mqtt.disconnect()


