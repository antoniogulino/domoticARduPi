#!/bin/bash
# usage
# ~/bin/start_rc.local.sh recordBroker-openhab "/usr/bin/python /home/openhabian/recordBroker/recordBroker-openhab.py"
#
# inserire sia in /etc/rc.local
# che anche in crontab -e
if [ "$1" ==  "" ] 
then
    ID=rtl
else
    ID=$1
fi

if [ "$2" ==  "" ] 
then
    COMANDO="~/rtl_433/build/src/rtl_433 -G -F csv | ~/rtl2me/rtl2me.pl"
else
    COMANDO=$2
fi

MINUTI=$(date +%M)
N_MINUTI=10
let "OGNI_N_MINUTI = $MINUTI % $N_MINUTI"


BROKER=openhab
TOPIC_ROOT=monitor/`hostname`
TOPIC_BASE=rc.local
TOPIC=$TOPIC_ROOT/$TOPIC_BASE/$ID
TMP_FILE=/media/ramdisk/$TOPIC_BASE-_-$ID.txt

ps -ef | grep "$ID" | grep -v $0 |grep -v grep > $TMP_FILE


#file_content=`head  -1 $TMP_FILE`
#file_content=$(head  -1 $TMP_FILE)
#file_content=$(grep "$COMANDO" $TMP_FILE|grep -v grep)

#if [ "$file_content" == "" ]
if [ -s $TMP_FILE ]
then
    if [ $OGNI_N_MINUTI -eq 0 ]
    then
        mosquitto_pub -h $BROKER -t $TOPIC -q 1 -f $TMP_FILE
    fi
else 
    mosquitto_pub -h $BROKER -t $TOPIC -q 1 -m "MISSING $COMANDO"
    sleep 1
    mosquitto_pub -h $BROKER -t $TOPIC -q 1 -m "TRY START $COMANDO"
    $COMANDO > /dev/null &
    sleep 1
    ps -ef | grep "$ID" | grep -v $0 |grep -v grep > $TMP_FILE
    if [ -s $TMP_FILE ]
    then
        mosquitto_pub -h $BROKER -t $TOPIC -q 1 -m "OK START $COMANDO > /dev/null $"
        sleep 1
        mosquitto_pub -h $BROKER -t $TOPIC -q 1 -f $TMP_FILE
    else
        mosquitto_pub -h $BROKER -t $TOPIC -q 1 -m "FAILED START $COMANDO > /dev/null $"
    fi
fi

