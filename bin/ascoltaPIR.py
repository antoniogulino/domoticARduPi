#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import csv
import RPi.GPIO as GPIO
import sys
import os

# uso l'identificazione BCM dei gpio
PIR = 21 # dove � attaccato il segnale del PIR, GPIO.29, pin=40
LED = 20 # dove � attaccato il led. GPIO.28, pin=38

GPIO.setmode(GPIO.BCM)
#GPIO.setup(PIR, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) 
GPIO.setup(PIR, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(LED, GPIO.OUT)

time_accensione_luce_prec = 0
time_foto_prec = 0

while False:
#    i=GPIO.input(PIR)
    i = 9
    lt = time.localtime()
    jahr, monat, tag, stunde, minute, sekunde = lt[0:6]
    orario =  "{0:4d}".format(jahr)+'{0:02d}'.format(monat)+' {0:02d}'.format(stunde)+':{0:02d}'.format(minute)+':{0:02d}'.format(sekunde)
    if i==0:                 #When output from motion sensor is LOW
        print "No intruders",orario,i
        GPIO.output(LED, 0)  #Turn OFF LED
        time.sleep(1)
    else:               #When output from motion sensor is HIGH
        print "Intruder detected",orario,i
        GPIO.output(LED, 1)  #Turn ON LED
        time.sleep(1)


def motion(pio):
    presente = GPIO.input(PIR)
    global time_accensione_luce_prec
    global time_foto_prec

    lt = time.localtime()

 
    if presente:
        # attivo l'azione PIR-on
        os.system("/home/tomi/action/PIR-on.py")
    else:
        # attivo l'azione PIR-off
        os.system("/home/tomi/action/PIR-off.py")

    if presente:
        os.system('/usr/bin/gpio -g write 20 1') # accendo il led
    else:
        os.system('/usr/bin/gpio -g write 20 0') # spengo il led
    #        os.system("/home/tomi/bin/433 1136067") # accendo luce zona divano/letto
    # spegner� il led al massimo 5 secondi dopo
    time_accensione_luce_prec = time.mktime(lt) + 5

    if (time.mktime(lt) - time_foto_prec > 30):
#        os.system('/home/tomi/bin/foto2tg.py &')
	    time_foto_prec = time.mktime(lt)
	    
    return


#GPIO.add_event_detect(PIR, GPIO.RISING, bouncetime=5000)
GPIO.add_event_detect(PIR, GPIO.BOTH, bouncetime=500)
GPIO.add_event_callback(PIR, motion)

try:
    while True:
        time.sleep(0.01)
	if (time.mktime(time.localtime()) - time_accensione_luce_prec > 1):
        # spengo la luce led    
	    os.system('/usr/bin/gpio -g write 20 0') 

#	if (time.mktime(time.localtime()) - time_accensione_luce_prec > 3):
#	    os.system('/usr/bin/gpio -g write 20 1') # accendo la luce led
#            time_accensione_luce_prec = time.mktime(time.localtime())

except KeyboardInterrupt:
    GPIO.cleanup()
    sys.exit()
