#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import csv
import RPi.GPIO as GPIO
import sys
import os
import random

inputDir = "/home/tomi/sensors"
IDsensore = "PIRsoggiorno"
IDanalogIn= "AnalogIn-raspi32"


# faccio una pausa di durata casuale, per evitare che troppe cosa avvengano all'inizio di un minuto
time.sleep(4 + 10*random.random()) # fa una pausa di 4 -14 secondi


lt = time.localtime()
jahr, monat, tag, stunde, minute, sekunde = lt[0:6]


filenameAnalogIn = inputDir + "/" + IDanalogIn + "-"+"{0:4d}".format(jahr)+'{0:02d}'.format(monat)+".tsv"
filename         = inputDir + "/" + IDsensore  + "-"+"{0:4d}".format(jahr)+'{0:02d}'.format(monat)+".tsv"

# verifico se è abbastanza buio da poter escludere che la luce in cucina è accesa
abbastanzaBuio = True
os.system('/usr/bin/tail -n 1 ' + filenameAnalogIn + ' > /media/ramdisk/' + IDanalogIn + '-ultimaRiga.tsv')
with open('/media/ramdisk/' + IDanalogIn + '-ultimaRiga.tsv','r') as f:
    # do per scontato che ci sia un'unica riga
    # dunque leggo la prima=ultima riga
    line = f.readline()
    [giorno,mese,anno,hh,min,sec,in0,in1,in2,in3] = line.split("\t")
    in2 = float(in2)
    if in2<15.:
        abbastanzaBuio = False
    
abbastanzaLuce = not abbastanzaBuio

ultimoPir = os.path.getmtime(filename)


#print(time.localtime(ultimoPir))
#print(lt)
#print(ultimoPir)
#print(time.mktime(lt))
#print((time.mktime(lt) -  ultimoPir)/60)
minutiPassatiUltimoPir = (time.mktime(lt) -  ultimoPir)/60

# se c'è abbastanza luce da poter sospettare che quella della cucina sia accesa
# e se sono passati almeno 5 minuti dall'ultimo PIR
# allora spengo la luce
# non faccio però nulla se sono passati più di 15 minuti
# per evitare di ingolfare la frequenza 433
# ma dopo 30 e dopo 60 minuti ci provo di nuovo
#print(abbastanzaLuce)
#print(minutiPassatiUltimoPir)
#print(5 < minutiPassatiUltimoPir)
#print(minutiPassatiUltimoPir < 15)
if abbastanzaLuce & ((abs(minutiPassatiUltimoPir-6)<2) | (abs(minutiPassatiUltimoPir-30)<2) | (abs(minutiPassatiUltimoPir-60)<2)):
    # spengo la luce
    os.system('/home/tomi/bin/433 C 0 ')
    os.system('/home/tomi/bin/433 A 0 ')
    os.system('/home/tomi/bin/433 C 0 ')
    os.system('/home/tomi/bin/433 A 0 ')
    os.system('/home/tomi/bin/433 C 0 ')
    os.system('/home/tomi/bin/433 A 0 ')
    os.system('/home/tomi/bin/433 C 0 ')
    os.system('/home/tomi/bin/433 B 0 ')

    
