#!/usr/bin/python3
#import Adafruit_DHT
import adafruit_dht
from board import *
import paho.mqtt.client as mqtt
from datetime import datetime
from time import gmtime, strftime, time
import os
import json
import csv
import socket

#DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN    = D4
DHT_PIN_nr = 4

broker = 'openhab'
port=1883
topic_root = "%s/dht22/%d" % (socket.gethostname(), DHT_PIN_nr)

client = mqtt.Client()
client.connect(broker, port)

dht22 = adafruit_dht.DHT22( DHT_PIN,use_pulseio=False)

umidita     = dht22.humidity
temperatura = dht22.temperature
#umidita, temperatura = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)

if umidita is not None and temperatura is not None:
    temperatura = "%1.1f" % temperatura
    umidita     = "%1.1f" % umidita

    unixtime = time()
    mygmtime = gmtime(unixtime)
    
    mytopic = topic_root
    my4json = {'timestamp' : strftime("%d-%m-%Y %H:%M:%S", mygmtime)
                ,'temp'     : temperatura
                ,'humidity' : umidita
                ,'unixtime' : round(unixtime)
                ,'yyyy' : strftime("%Y",mygmtime)
                ,'mm' : strftime("%m",mygmtime)
                ,'dd' : strftime("%d",mygmtime)
                ,'hour' : strftime("%H",mygmtime)
                ,'min' : strftime("%M",mygmtime)
                ,'sec' : strftime("%S",mygmtime)
                ,'modelID' : DHT_PIN_nr
                ,'model' : "dht22"
                ,'gateway' : socket.gethostname()
                ,'script'  : __file__
                ,'status'  : "OK"
                }
    client.publish(topic=mytopic, payload=json.dumps(my4json), qos = 1, retain = True)
    
    mytopic = topic_root +  "/temp"
    client.publish(topic=mytopic, payload=temperatura, qos = 1, retain = True)
    
    mytopic = topic_root + "/humidity"
    client.publish(topic=mytopic, payload=umidita, qos = 1, retain = True)

    mytopic = topic_root + "/status"
    client.publish(topic=mytopic, payload="OK", qos = 1, retain = True)
    print("Temp={0:1s}*C  Humidity={1:1s}%".format(temperatura, umidita))
else:
    mytopic = topic_root + "/status"
    client.publish(topic=mytopic, payload="KO", qos = 1, retain = True)
    print("Failed to retrieve data from humidity sensor")
