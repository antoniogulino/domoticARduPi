#!/usr/bin/perl
use JSON;
use Sys::Hostname;
use Net::MQTT::Simple;

$gateway = hostname; 
$unixtime   = time;
@gmt        = gmtime;
$gmtISO8601 = sprintf("%4d-%02d-%02dT%02d:%02d:%02dZ" , $gmt[5]+1900 , $gmt[4]+1 , @gmt[(3,2,1,0)] );


%param = (); 
$param{$_}=0 foreach qw(--mqtt --tsv --json --broker --port --topic --by);

@parametri = @ARGV; @ARGV = ();
$param{$_}=1 foreach @parametri;

foreach my $tmp_param (sort keys %param) {
    if ( $tmp_param =~ /^(.+?)=(.+)$/ ) {
        $param{$1} = $2;
    }
}

if ($param{'--broker'}) {
    $param{'--mqtt'} = 1 unless $param{'--mqtt'};
}
if ($param{'--mqtt'}) {
# default
    $param{'--broker'} = 'openhab'  unless $param{'--broker'};
    $param{'--topic' } = 'wlanscan' unless $param{'--topic' };
    $param{'--port'  } = '1883'     unless $param{'--port'  };

# default
    $param{'--broker'} = 'openhab'  if $param{'--broker'} eq 1;
    $param{'--topic' } = 'wlanscan' if $param{'--topic' } eq 1;
    $param{'--port'  } = '1883'     if $param{'--port'  } eq 1;
}


#################################



#################################

$scan{'gateway'} = $gateway;
$scan{'script'}  = $0;
$scan{'time'}{'iso'}  = $gmtISO8601;   
$scan{'time'}{'unix'} = $unixtime;   
#%scan = ();
while (<>) {
    s/  +/ /g; # mi evito di indicare sempre il + nei blank
    if (/^(\S*?) Scan completed.*:.*$/) { $interface = $1; }
    if (/ +Cell (\d+) *- *Address: ([0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}).*$/) {
        $cell = $1;
        $mac  = $2;
    }
    if (/ Address: ([0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}).*$/) {$mac = $1}
    if (/^ Channel:(\d+)/                              ) { $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{'Channel'}=$1; }
    elsif (/^ Frequency:(\d+\.?\d+) GHz .Channel \d+./     ) { $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{'GHz'    }=$1; }
    elsif (/^ ESSID:"(.+)"/                               ) { $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{'ESSID'  }=$1; }
    elsif (/^ Mode:(.+)/                                  ) { $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{'Mode'  }=$1; }
    elsif (/^ Quality=(\d+\/\d+) Signal level=(.?\d+) dBm/) { 
        $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{'Quality'  }=$1; 
        $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{'dBm'      }=$2; 
    }
    elsif (/^ Extra: (Last beacon): (\d+ms)/                ) { $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{ $1 } = $2; }
    elsif (/^ (Encryption key):(\w+)/                       ) { $scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}{ $1 } = $2; }


}



if ($param{'--json'}) {
    open F, ">$param{'--json'}" unless $param{'--json'} eq 1;
    if ($param{'--json'} eq 1) {
        print encode_json \%scan;
    } else {
        print F encode_json \%scan;
    }
    close F unless $param{'--json'} eq 1;
}


if ($param{'--tsv'}) {
    open F, ">$param{'--tsv'}" unless $param{'--tsv'} eq 1;
    foreach $interface (keys %{$scan{'interface'}}) {
        foreach $mac (sort keys %{$scan{'interface'}{$interface}{'mac'}}) {
            foreach $cell (sort keys %{$scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}}) {
                %e = %{$scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}};
                $riga = join("\t", $mac, @e{("Channel","GHz","Mode","dBm","Quality","Last beacon","Encryption key","ESSID")},$cell,$interface) . "\n";
                if ($param{'--tsv'} eq 1) {
                    print $riga;
                } else {
                    print F $riga;
                }
#                print "$interface\t$cell\t$e{'Channel'}\t$e{'GHz'}\t$mac\t$e{'Mode'}\t$e{'dBm'}\t$e{'Last beacon'}\t$e{'Encryption key'}\t$e{'ESSID'}\n";
            }
        }
    }
    close F unless $param{'--tsv'} eq 1;

}

if ($param{'--mqtt'}) {
    # il topic per identificare il device trovato si basa sulla coppia "ESSID_Mac"
    # il topic per identificare il device gateway si basa sulla coppia "gateway_interface"
    my $mqtt = Net::MQTT::Simple->new($param{"--broker"});
    my $gateway    = $scan{'gateway'};
    my $script     = $scan{'script'};

    foreach my $interface (keys %{$scan{'interface'}}) {

        my $gatewayID = sprintf("%1s_%1s", $gateway,$interface);

        foreach my $mac (sort keys %{$scan{'interface'}{$interface}{'mac'}}) {
            foreach $cell (sort keys %{$scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}}) {
                my %e = %{$scan{'interface'}{$interface}{'mac'}{$mac}{'cell'}{$cell}};

                my $deviceID = sprintf( "%1s_%1s" , $mac , $e{"ESSID"} );

                my %json =();

                $json{'mac'} = $mac;
                for my $var (( "Mode","Encryption key","ESSID")) {
                    $json{$var} = $e{$var};
                }
                $json{'gateway'}{'host'}      = $gateway;
                $json{'gateway'}{'interface'} = $interface;
                $json{'gateway'}{'script'}    = $script;
                $json{'gateway'}{'time'}{'iso'}  = $gmtISO8601;   
                $json{'gateway'}{'time'}{'unix'} = $unixtime;   

                $json{'channel'}{'Cell'} = $cell;
                for my $var (("Channel","GHz","dBm","Quality","Last beacon")) {
                    $json{'channel'}{$var} = $e{$var};
                }

                my $topic = join("/" , $param{'--topic'}, $deviceID, $gatewayID);
                my $message = encode_json \%json;
                $mqtt->retain($topic => $message);
                
                #$mqtt->publish("topic/here" => "Message here");
                #$mqtt->retain( "topic/here" => "Message here");


            }
        }
    }
} # if --mqtt
 
    



1;
