#!/usr/bin/python3
#import Adafruit_DHT
#import adafruit_dht
#from board import *
from gpiozero import CPUTemperature
import paho.mqtt.client as mqtt
from datetime import datetime
from time import gmtime, strftime, time
import os
import json
import csv
import socket

cpu = CPUTemperature()

broker = 'openhab'
port=1883
topic_root = "%s/cpu" % (socket.gethostname())

client = mqtt.Client()
client.connect(broker, port)


temperatura = cpu.temperature

if temperatura is not None:
    temperatura = "%1.1f" % temperatura

    unixtime = time()
    mygmtime = gmtime(unixtime)
    
    mytopic = topic_root
    my4json = {'timestamp' : strftime("%d-%m-%Y %H:%M:%S", mygmtime)
                ,'temp'     : temperatura
                ,'unixtime' : round(unixtime)
                ,'yyyy' : strftime("%Y",mygmtime)
                ,'mm' : strftime("%m",mygmtime)
                ,'dd' : strftime("%d",mygmtime)
                ,'hour' : strftime("%H",mygmtime)
                ,'min' : strftime("%M",mygmtime)
                ,'sec' : strftime("%S",mygmtime)
                ,'model' : "cpu"
                ,'gateway' : socket.gethostname()
                ,'script'  : __file__
                ,'status'  : "OK"
                }
    client.publish(topic=mytopic, payload=json.dumps(my4json), qos = 1, retain = True)
    
    mytopic = topic_root +  "/temp"
    client.publish(topic=mytopic, payload=temperatura, qos = 1, retain = True)
    
    mytopic = topic_root + "/status"
    client.publish(topic=mytopic, payload="OK", qos = 1, retain = True)
    print("Temp={0:1s}*C".format(temperatura ))
else:
    mytopic = topic_root + "/status"
    client.publish(topic=mytopic, payload="KO", qos = 1, retain = True)
    print("Failed to retrieve data from cpu sensor")
