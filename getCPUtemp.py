#!/usr/bin/python
from datetime import datetime
from time import gmtime, strftime
import os

def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    return(res.replace("temp=","").replace("'C\n",""))

timeStamp = strftime("%d\t%m\t%Y\t%H\t%M\t%S", gmtime()) # UTC

riga = timeStamp + "\t" + getCPUtemperature()

print riga

