#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import csv
import RPi.GPIO as GPIO
import sys
import os


# dove salvare i dati
outputDir = "/home/tomi/sensors"
inputDir = outputDir
IDsensore = "PIRsoggiorno"
IDanalogIn= "AnalogIn-raspi32"

lt = time.localtime()
jahr, monat, tag, stunde, minute, sekunde = lt[0:6]

filenameAnalogIn =  inputDir + "/" + IDanalogIn + "-"+"{0:4d}".format(jahr)+'{0:02d}'.format(monat)+".tsv"
filename         = outputDir + "/" + IDsensore  + "-"+"{0:4d}".format(jahr)+'{0:02d}'.format(monat)+".tsv"
orario =  "{0:2d}".format(tag)+'-{0:02d}'.format(monat)+'-{0:04d}'.format(jahr)+' {0:02d}'.format(stunde)+':{0:02d}'.format(minute)+':{0:02d}'.format(sekunde)

# verifico se � abbastanza buio da dover accendere la luce
abbastanzaBuio = True
os.system('/usr/bin/tail -n 1 ' + filenameAnalogIn + ' > /media/ramdisk/' + IDanalogIn + '-ultimaRiga.tsv')
with open('/media/ramdisk/' + IDanalogIn + '-ultimaRiga.tsv','r') as f:
    # do per scontato che ci sia un'unica riga
    # dunque leggo la prima=ultima riga
    line = f.readline()
    [giorno,mese,anno,hh,min,sec,in0,in1,in2,in3] = line.split("\t")
    if in1<230:
        abbastanzaBuio = False
    

if abbastanzaBuio: 
    # accendo la luce
    os.system('/home/tomi/bin/433 C 1 ')
    os.system('/home/tomi/bin/433 C 1 ')
    os.system('/home/tomi/bin/433 C 1 ')
    os.system('/home/tomi/bin/433 C 1 ')

  
# se il file mensile non esiste ancora, lo creo mettendo nome dei campi
if (os.path.isfile(filename)==False):
    with open(filename, "a") as tsv:
        tw = csv.writer(tsv, delimiter="\t", lineterminator="\n")
        tw.writerow(["giorno","mese","anno","hh","min","sec","PIR"])

with open(filename, "a") as tsv:
    tw = csv.writer(tsv, delimiter="\t", lineterminator="\n", quotechar='"')
    tw.writerow([tag, monat, jahr, stunde, minute, sekunde, 1])


