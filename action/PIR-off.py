#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import csv
import RPi.GPIO as GPIO
import sys
import os


# dove salvare i dati
outputDir = "/home/tomi/sensors"
IDsensore = "PIRsoggiorno"

lt = time.localtime()
jahr, monat, tag, stunde, minute, sekunde = lt[0:6]

filename         = outputDir + "/" + IDsensore  + "-"+"{0:4d}".format(jahr)+'{0:02d}'.format(monat)+".tsv"
orario =  "{0:2d}".format(tag)+'-{0:02d}'.format(monat)+'-{0:04d}'.format(jahr)+' {0:02d}'.format(stunde)+':{0:02d}'.format(minute)+':{0:02d}'.format(sekunde)

# spengo la luce
#os.system('/home/tomi/bin/433 C 0 ')
#os.system('/home/tomi/bin/433 C 0 ')
#os.system('/home/tomi/bin/433 C 0 ')
#os.system('/home/tomi/bin/433 C 0 ')

  
# se il file mensile non esiste ancora, lo creo mettendo nome dei campi
if (os.path.isfile(filename)==False):
    with open(filename, "a") as tsv:
        tw = csv.writer(tsv, delimiter="\t", lineterminator="\n")
        tw.writerow(["giorno","mese","anno","hh","min","sec","PIR"])

with open(filename, "a") as tsv:
    tw = csv.writer(tsv, delimiter="\t", lineterminator="\n", quotechar='"')
    tw.writerow([tag, monat, jahr, stunde, minute, sekunde, 0])


