#include <stdio.h>
#include<wiringPi.h>
#include<time.h>

#define Data 26

char DHT11_Read(char *temperature,char *humidity)	   
{                 
	unsigned char t,i,j;
	unsigned char buf[5]={0,0,0,0,0};
	pinMode(Data,OUTPUT); 	    //SET OUTPUT
	pullUpDnControl(Data,PUD_UP);
    digitalWrite(Data,0); 	    //Data=0
    delay(20);         	        //Pull down Least 18ms
    digitalWrite(Data,1); 	    //Data =1
	delayMicroseconds(30);     	//Pull up 20~40us
	pinMode(Data,INPUT);        //SET INPUT
	
    while (digitalRead(Data) && t <100)//DHT11 Pull down 80us
	{
		t++;
		delayMicroseconds(1);
	};	 
	if(t >= 100)return 1;
	t = 0;
    while (!digitalRead(Data) && t<100)//DHT11 Pull up 80us
	{
		t++;
		delayMicroseconds(1);
	};
	if(t >= 100)return 1;	    
	for(i=0;i < 5;i++)
	{
		buf[i] = 0;
		for(j=0;j<8;j++)
		{
			buf[i] <<= 1;
			t = 0;
			while(digitalRead(Data) && t < 100)
			{
				t++;
				delayMicroseconds(1);
			}
			if(t >= 100) return 1;
			t = 0;
			while(!digitalRead(Data) && t <100)
			{
				t++;
				delayMicroseconds(1);
			}
			if(t >= 100) return 1;
			delayMicroseconds(40);
			if(digitalRead(Data))
				buf[i] += 1;
		}
	}
	if(buf[0]+buf[1]+buf[2]+buf[3]!=buf[4])return 2;
	*humidity = buf[0];
	*temperature =buf[2];
	return 0;
} 


int main(void)
{
	char temperature;  	    
	char humidity; 
	char value = 1;
        time_t now;
	struct tm *timenow;

	if (wiringPiSetup() < 0)return 1;
	
  	while(value != 0)
	{
        	time(&now);
		timenow = localtime(&now);

		value = DHT11_Read(&temperature,&humidity);	
		if(value == 0) 
		{ 
			printf("%d\t",timenow->tm_mday);
			printf("%d\t",timenow->tm_mon + 1);
			printf("%d\t",timenow->tm_year + 1900);
			printf("%d\t",timenow->tm_hour);
			printf("%d\t",timenow->tm_min);
			printf("%d\t",timenow->tm_sec);
			printf("%d\t",temperature);
			printf("%d\n",humidity);
		} else {
 		        delay(1000);
		}
	}
}

