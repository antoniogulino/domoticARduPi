#!/usr/bin/python
from time import gmtime, strftime

from BMP180 import BMP180

timeStamp = strftime("%d\t%m\t%Y\t%H\t%M\t%S", gmtime())

# Initialise the BMP085 and use STANDARD mode (default value)
#bmp = BMP085(0x77, debug=True)
bmp = BMP180()

# To specify a different operating mode, uncomment one of the following:
# bmp = BMP085(0x77, 0)  # ULTRALOWPOWER Mode
# bmp = BMP085(0x77, 1)  # STANDARD Mode
# bmp = BMP085(0x77, 2)  # HIRES Mode
# bmp = BMP085(0x77, 3)  # ULTRAHIRES Mode
temp = bmp.read_temperature()

# Read the current barometric pressure level
pressure = bmp.read_pressure()

# To calculate altitude based on an estimated mean sea level pressure
# (1013.25 hPa) call the function as follows, but this won't be very accurate
altitude = bmp.read_altitude()

# To specify a more accurate altitude, enter the correct mean sea level
# pressure level.  For example, if the current pressure level is 1023.50 hPa
# enter 102350 since we include two decimal places in the integer value
# NB: 440mslm corrispondono a 961,5050261 hPa
# altitude = bmp.read_altitude(102350)

riga = timeStamp 
riga = riga + "\t%.1f" % temp
riga = riga + "\t%.2f" % (pressure / 100.0)
riga = riga + "\t%.0f" % altitude

print riga
