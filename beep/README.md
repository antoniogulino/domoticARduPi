Requisiti
=========
Hardware
--------
* Raspberry Pi
* Pioneer600 della WaveShare

Software
--------
Per la versione python servono le librerie 
* smbus
* time
* RPi.GPIO

Per la versione C servono le librerie
* wiringPi.h
* wiringPiI2C.h

Funzionamento
=============
Emette un suono di 1/10 sec dal buzzer del Pioneer600.

Programmazione
==============
La versione Python è un versione modificata e fortemente semplificata di `pcf8574.py`
presente nella directory `~/Pioneer600/PCF8574/python/`.

La versione C è una versione modificata e fortemente semplificata di `pcf8574.c` 
presente nella directory `~/Pioneer600/PCF8574/wiringPi/`.