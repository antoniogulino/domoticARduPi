#include <wiringPi.h>  
#include <wiringPiI2C.h>

#define PCF8574_Address 0x20


int main()  
{  

    if (wiringPiSetup() < 0)return 1;  
    int fd = wiringPiI2CSetup(PCF8574_Address); 
    
    wiringPiI2CWrite(fd,0x7F); // beep on
    delay(100);	
    wiringPiI2CWrite(fd,0x80); // beep off
    return 0;
}  
