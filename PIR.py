#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import csv
import RPi.GPIO as GPIO
import sys

# uso l'identificazione BCM dei gpio
PIR = 20 # dove � attaccato il segnale del PIR, GPIO.28, pin=38
LED = 26 # il led_1 di Pioneer600. GPIO.25, pin=37

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(LED, GPIO.OUT)

def motion(pio):

    lt = time.localtime()
    jahr, monat, tag, stunde, minute, sekunde = lt[0:6]

    with open("/home/pi/sensors" + "/PIR.tsv", "a") as tsv:
        tw = csv.writer(tsv, delimiter="\t", lineterminator="\n", quotechar='"')
        tw.writerow([tag, monat, jahr, stunde, minute, sekunde, 1])
    # per un secondo si accende la luce rossa incorporata in Pioneer
    GPIO.output(LED,GPIO.HIGH)
    time.sleep(1)
    GPIO.output(LED,GPIO.LOW)
    time.sleep(1)
    GPIO.output(LED,GPIO.HIGH)
    time.sleep(1)
    GPIO.output(LED,GPIO.LOW)
    time.sleep(1)
    GPIO.output(LED,GPIO.HIGH)
    time.sleep(1)
    GPIO.output(LED,GPIO.LOW)
    return


GPIO.add_event_detect(PIR, GPIO.RISING)
GPIO.add_event_callback(PIR, motion)

try:
    while True:
        time.sleep(0.5)
except KeyboardInterrupt:
    GPIO.cleanup()
    sys.exit()
