Riassunto
=========
I seguenti file richiedono l'installazione (hardware e software) di Pioneer600
della WaveShare (http://www.waveshare.com/wiki/Pioneer600)
* getDHT11 (C++, derivato da ~/Pioneer600/Sensor/Temperature-Humidity_Sensor)
* getAnalogIn0123 (C++, derivato da  ~/Pioneer600/Sensor/Flame_Sensor)
* get1wireTemp.py
* getBMP180.py

I seguenti file vengono lanciati con il boot della macchina (/etc/rc.local)
* PIR.py

I seguenti file vengono lanciati ad intervalli (crontab -e) e il loro output messo in un file tsv con quasi lo stesso nome (p.es. ~/bin/getDHT11 >> ~/sensors/DHT11.tsv
* getAnalogIn0123
* getAnalogIn.py
* getDHT11
* getCPUtemp.py
* getBMP180.py
* get1wireTemp.py

I seguenti file richiedono inoltre broker MQTT
* getAnalogIn.py

Gli script
==========

getAnalogIn0123 - leggere valori analogici, p.es. sensore luce
---------------
Legge i 4 valori analogici tramite Pioneer600 e ha come output il momento attuale (in UTC) e i 4 valori, separati da tabulatore.

Probabilmente bastano pochi adattamenti per usare questo script anche senza Pioneer (ma chiaramente con qualche scheda AD/DA).


getAnalogIn.py - leggere valori analogici, p.es. sensore luce
--------------
Legge i 4 valori analogici presenti un una sceda AD/DA e ha come output il momento attuale e i quattro valori
* come print: i valori separati da tabulatore
* come MQTT: ciascun valore e timeStamp in un topic ciascuno


getDHT11 - leggere temperatura e umidità
--------
Legge la temperatura e l'umidità del sensore DHT11 collegato al Pioneer600  e ha come output il momento attuale (in UTC) e i 2 valori, separati da tabulatore.

Di per se non necessita del Pioneer600.

A seconda della lunghezza dei fili di collegamento, misura la temperatura e l'umidità della stanza.

getCPUtemp.py - monitorare la temperatura del processore
-------------
Legge la temperatura del processore facendo semplicemente una chiamata di sistema (vcgencmd measure_temp). Ha come output il momento attuale (in UTC) e la temperatura.

getBMP180.py - leggere la pressione atmosferica e la temperatura
------------
Legge la pressione atmosferica e la temperatura, cosí come forniti dal chip BMP180 presente sul Pioneer600. Ha come output il momento attuale (in UTC) e i 2 valori, separati da tabulatore.

Di per se funziona con leggero adattamento anche per una qualsiasi scheda con il chip BMP180.

La temperatura fa riferimento allo spazio tra Raspberry e Pioneer600

get1wireTemp.py - leggere la temperatura
---------------
Legge la temperatura misurata tramite il sensore 1wire collegato al Pioneer600.
Ha come output il momento attuale (in UTC) e la temperatura.

Di per se dovrebbe funzionare con pochi adattamenti anche senza il Pioneer600.

Se non si usano fili di collegamento che allontanano il sensore, allora viene misurata la temperatura vicino al Pioneer600,
ma esternamente.


PIR.py - Rilevamento di movimento
------
Ogni volta che il PIR (sensore di movimento tramite rilevamento passivo degli infrarossi) viene attivato, viene memorizzato nel file apposito (~/seonsors/PIR.tsv) il momento.

Lo scripts PIR.py viene lanciato all'avvio della macchina modificando /etc/rc.local e dovrebbe rimanere funzionante per tutto il tempo.